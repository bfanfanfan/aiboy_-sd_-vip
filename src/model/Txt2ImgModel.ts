// 文生图api /sdapi/v1/txt2img 请求参数
export interface Txt2ImgReq {
    prompt: string,
    negative_prompt: string,
    width: number,
    height: number,
    steps: number,
    cfg_scale: number,
    sampler_name: string,
    batch_size: number,
    seed: number,
    save_images: boolean,
    alwayson_scripts: object | {}
}

// 文生图api /sdapi/v1/txt2img 响应结果
export interface Txt2ImgRes {
    images: [],
    parameters: string,
    info: string
}

// 采样方法api /sdapi/v1/samplers 响应结果
export interface Txt2ImgSamplers {
    name: string,
    aliases: string[],
    options: {}
}

// ControleNet 请求参数
export interface ControlNetReq {
    input_image: string,
    module: string,
    model: string,
    lowvram: boolean,
    pixel_perfect: boolean,
    weight: number,
    guidance_start: number,
    guidance_end: number,
    control_mode: number,
    resize_mode: number,
    processor_res: number,
    threshold_a: number,
    threshold_b: number
}


import { useStorage } from '@vueuse/core'

// 将所有组件的输入保存在浏览器的 localStorage 中，key为 txt2imgReqStorage
export const txt2imgReqStorage = useStorage<Txt2ImgReq>('txt2imgReq', {})

// 将文生图调用结果图片保存在浏览器的 localStorage 中，key为 txt2img_imgs
export const txt2imgResultStorage = useStorage<string[]>('txt2img_imgs', [])