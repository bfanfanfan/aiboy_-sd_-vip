// ControlNet的api /controlnet/module_list 的响应结果
export interface ControleNetModuleRes {
    module_list: []
}

// ControlNet的api /controlnet/model_list 的响应结果
export interface ControlNetModelRes {
    model_list: []
}