import { createApp } from 'vue'

// 引入ant-design-vue 
import 'ant-design-vue/dist/antd.css'
import antdv from 'ant-design-vue'

import main from './views/main.vue'

// 将ant-design-vue进行注册
createApp(main).use(antdv).mount('#app')
