import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // 服务端代理
  server: {
    // 监听主机 127.0.0.1，如果是 0.0.0.0 则代表所有ip都可以访问该前端项目
    host: "127.0.0.1",
    // 将 /sdapi、 /controlnet 开头的请求都代理到服务端地址 http://127.0.0.1:7860
    proxy: {
      '/sdapi': {
        target: 'http://127.0.0.1:7860',
        changeOrigin: true
      },
      '/controlnet': {
        target: 'http://127.0.0.1:7860',
        changeOrigin: true
      }
    }
  }
})
